import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home from "./components/Home";
import About from './components/About';
import Result from "./components/Result";
import History from "./components/History";
import NotFound from "./components/NotFound";
import Dataset from "./components/Dataset";
import GitlabCorner from "./components/GitlabCorner";

import 'nprogress/nprogress.css';
import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';

class App extends Component {
  render() {
    return (
      <Router className="mb-5">
        <Switch>
          <Route path="/about" component={About}/>
          <Route path="/dataset" component={Dataset}/>
          <Route path="/history" component={History}/>
          <Route path="/result" component={Result}/>
          <Route path="/" exact component={Home}/>
          <Route path="*">
            <NotFound/>
          </Route>
        </Switch>
        {
          <GitlabCorner/>
        }
      </Router>
    )
  }

}

export default App;