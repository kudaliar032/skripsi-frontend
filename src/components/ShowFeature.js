import React, {Component} from 'react';
import {IoIosHelpCircle} from 'react-icons/io';
import $ from 'jquery';

const UseHttps = props => {
  if (props.https === -1) {
    return <span className="badge badge-success">Secured</span>
  } else if (props.https === 0) {
    return <span className="badge badge-warning">Not SSL</span>
  } else {
    return <span className="badge badge-danger">Not secured</span>
  }
};

const AlexaRank = props => {
  if (props.alexarank === -1) {
    return <span className="badge badge-success">Less than 10M</span>
  } else if (props.alexarank === 0) {
    return <span className="badge badge-warning">More than 10M</span>
  } else {
    return <span className="badge badge-danger">Don't have</span>
  }
};

const IpAddress = props => {
  return props.ip === 1 ?
    <span className="badge badge-danger">Yes</span> :
    <span className="badge badge-success">No</span>;
};

const SymbolAt = props => {
  return props.simbol_at === 1 ?
    <span className="badge badge-danger">Yes</span> :
    <span className="badge badge-success">No</span>;
};

const Organization = props => {
  return props.organization === 1 ?
    <span className="badge badge-success">Yes</span> :
    <span className="badge badge-danger">No</span>;
};

const Private = props => {
  return props.private === 1 ?
    <span className="badge badge-danger">Yes</span> :
    <span className="badge badge-success">No</span>;
};

class ShowFeature extends Component {
  showHelp() {
    $('#featureColorLegend').modal('show');
  }

  render() {
    const {ip, simbol_at, afiks, usia_domain, organization, privasi, url_long, url_https, alexarank, js, page_score} = this.props.features;
    return (
      <div className={`row ${this.props.className}`}>
        <div className="col-6 col-lg" align="center">
          <p>IP Address<br/><IpAddress ip={ip}/></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>Symbol @<br/><SymbolAt simbol_at={simbol_at}/></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>Affix<br/><span className="badge badge-info">{afiks}</span></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>Domain Ages<br/><span className="badge badge-info">{usia_domain}</span></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>Organization<br/><Organization organization={organization}/></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>Private<br/><Private private={privasi}/></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>URL Length<br/><span className="badge badge-info">{url_long}</span></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>HTTPs<br/><UseHttps https={url_https}/></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>Alexa Rank<br/><AlexaRank alexarank={alexarank}/></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>JavaScript<br/><span className="badge badge-info">{js}</span></p>
        </div>
        <div className="col-6 col-lg" align="center">
          <p>Google PSI<br/><span className="badge badge-info">{page_score}</span></p>
        </div>
        <div onClick={this.showHelp.bind(this)}>
          <IoIosHelpCircle size={25} color={'#828282'}/>
        </div>

        <div className="modal fade" id="featureColorLegend" tabIndex="-1" role="dialog"
             aria-labelledby="featureColorLegendLabel"
             aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="featureColorLegendLabel">Feature Color Legend</h5>
              </div>
              <div className="modal-body">
                <table>
                  <tbody>
                  <tr valign="top">
                    <td className="pr-3"><span className="badge badge-success">Green</span></td>
                    <td>Green color for feature like non-phishing site</td>
                  </tr>
                  <tr valign="top">
                    <td className="pr-3"><span className="badge badge-warning">Yellow</span></td>
                    <td>Yellow color for feature between non-phishing and phishing site</td>
                  </tr>
                  <tr valign="top">
                    <td className="pr-3"><span className="badge badge-danger">Red</span></td>
                    <td>Red color for feature like phishing site</td>
                  </tr>
                  <tr valign="top">
                    <td className="pr-3"><span className="badge badge-info">Blue</span></td>
                    <td>Blue color for numeric feature</td>
                  </tr>
                  </tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ShowFeature;