import React, {Component} from 'react';
import {Pie} from 'react-chartjs-2';

const nonPhishingColor = '#28a745';
const phishingColor = '#dc3545';

class ShowProba extends Component {
  render() {
    return (
      <div>
        <Pie
          data={{
            datasets: [{
              data: [this.props.proba[0], this.props.proba[1]],
              backgroundColor: [nonPhishingColor, phishingColor]
            }],
            labels: ['Non-phishing', 'Phishing']
          }}
          options={{
            elements: {
              arc: {
                borderWidth: 0
              }
            },
            layout: {
              padding: {
                top: 20,
                bottom: 20
              }
            }
          }}
        />
      </div>
    );
  }
}

export default ShowProba;