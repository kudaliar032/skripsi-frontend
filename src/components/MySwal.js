import Swal from "sweetalert2";

export const UrlAlert = () => {
  return Swal.fire({
    heightAuto: false,
    title: 'Error!',
    text: 'URL not found or invalid',
    icon: 'error',
    confirmButtonText: 'OK'
  });
}

export const DetectAlert = () => {
  return Swal.fire({
    heightAuto: false,
    title: 'Error!',
    text: 'URL detection error, try again',
    icon: 'error',
    confirmButtonText: 'OK'
  }).then(({value}) => {
    if (value) {
      window.location.replace('/');
    }
  })
}