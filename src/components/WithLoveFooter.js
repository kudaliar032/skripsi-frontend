import React, {Component} from 'react';
import {FaHeart} from 'react-icons/fa';

class WithLoveFooter extends Component {
  render() {
    return (
      <footer className="mt-auto bd-highlight">
        <nav className="navbar navbar-expand navbar-light">
          <div className="navbar-nav ml-auto mr-auto">
            <p>Made with <FaHeart color={'#e25555'}/> by <a target="_blank" rel="noopener noreferrer" href="https://github.com/kudaliar032">Aditya Rahman</a></p>
          </div>
        </nav>
      </footer>
    );
  }
}

export default WithLoveFooter;