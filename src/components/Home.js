import React, {Component} from 'react';
import logo from '../img/with-text.svg';
import HomeNavBar from "./HomeNavBar";
import {useHistory} from 'react-router-dom';
import axios from 'axios';
import {BeatLoader} from 'react-spinners';
import WithLoveFooter from "./WithLoveFooter";
import {UrlAlert} from "./MySwal";

const DetectButton = props => {
  const history = useHistory();

  async function clickDetect() {
    try {
      props.changeLoading(true);
      const url = encodeURIComponent(props.urlValue);
      await axios.get(`/url-check?url=${url}`);
      props.changeLoading(false);
      history.push(`/result?url=${url}`);
    } catch (e) {
      console.log(e);
      props.changeLoading(false);
      UrlAlert();
    }
  }

  return (
    <button
      className="btn btn-secondary"
      type="submit"
      id="input-url-site"
      onClick={clickDetect}
      disabled={props.urlValue === ""}
    >Detect
    </button>
  );
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: "",
      isLoading: false
    }
  }

  changeUrl(e) {
    this.setState({url: e.target.value});
  }

  changeLoading(state) {
    this.setState({isLoading: state});
  }

  render() {
    return (
      <div className="d-flex flex-column h-100 bd-highlight">
        <HomeNavBar/>
        <div className="container-fluid">
          <div className="row justify-content-center mt-5">
            <div className="col-10 col-lg-5 text-center">
              <img src={logo} alt="Phishycho Logo" className="img-fluid"/>
            </div>
          </div>
          <div className="row justify-content-center mt-5">
            <div className="col-12 col-lg-7">
              <div className="input-group input-group-lg">
                <input
                  type="text"
                  value={this.state.url}
                  onChange={this.changeUrl.bind(this)}
                  className="form-control"
                  placeholder="Type URL here..."
                  aria-label="Type URL here..."
                  aria-describedby="input-url-site"
                />
                <div className="input-group-append">
                  <DetectButton changeLoading={this.changeLoading.bind(this)} urlValue={this.state.url}/>
                </div>
              </div>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-12" align="center">
              <BeatLoader
                color={"#16A39C"}
                size={10}
                margin={2}
                loading={this.state.isLoading}
              />
            </div>
          </div>
        </div>
        <WithLoveFooter/>
      </div>
    )
  }
}

export default Home;