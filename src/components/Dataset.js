import React, {Component} from "react";
import NavBar from "./NavBar";
import $ from 'jquery';
import 'datatables.net-bs4';
import WithLoveFooter from "./WithLoveFooter";

class Dataset extends Component {
  componentDidMount() {
    $('#phishing-dataset').DataTable({
      ajax: `${process.env.REACT_APP_BACKEND_API}/dataset`,
      columns: [
        {data: "ip"},
        {data: "simbol_at"},
        {data: "afiks"},
        {data: "usia_domain"},
        {data: "organization"},
        {data: "privasi"},
        {data: "url_long"},
        {data: "url_https"},
        {data: "alexarank"},
        {data: "js"},
        {data: "page_score"},
        {
          className: 'text-center',
          data: ({status}) => {
            return `<span class="badge badge-${status === 'phishing' ? 'danger' : 'success'}">${status}</span>`
          }
        }
      ],
      processing: true,
      serverSide: true,
      searching: false,
      ordering: false,
      pageLength: 7,
      lengthChange: false
    })
  }

  render() {
    return (
      <div className="d-flex flex-column h-100 bd-highlight">
        <NavBar/>
        <main role="main" className="flex-fill bd-highlight">
          <div className="container-fluid px-5">
            <div className="row">
              <div className="col">
                <h2 className="text-center"><b>Dataset</b></h2>
                <div className="table-responsive pt-2">
                  <table className="table table-striped table-bordered text-center" id="phishing-dataset">
                    <thead>
                    <tr>
                      <th>IP Address</th>
                      <th>Symbol @</th>
                      <th>Affix</th>
                      <th>Domain Ages</th>
                      <th>Organization</th>
                      <th>Private</th>
                      <th>URL Length</th>
                      <th>HTTPs</th>
                      <th>Alexa Rank</th>
                      <th>JavaScript</th>
                      <th>Google PSI</th>
                      <th style={{minWidth: "7rem"}}>Status</th>
                    </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </main>
        <WithLoveFooter/>
      </div>
    )
  }
}

export default Dataset;