import React, {Component} from 'react';
import NavBar from "./NavBar";
import WithLoveFooter from "./WithLoveFooter";
import $ from "jquery";
import axios from 'axios';
import Chart from 'chart.js';
import '../css/my-styles.css';
import {FaExternalLinkAlt} from 'react-icons/fa';

const FeatureDetail = ({id}) => (
  <div className="col text-center" id={id}>
    <strong className="feature-title">Title</strong><br/>
    <span className="badge badge-secondary">Feature</span>
  </div>
)

const featureList = ['ip', 'simbol_at', 'afiks', 'usia_domain', 'organization', 'privasi', 'url_long', 'url_https', 'alexarank', 'js', 'page_score'];

class History extends Component {
  // probability chart
  componentDidMount() {
    const ctx = document.getElementById('probability-chart').getContext('2d');
    const chart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels: ['Non-phishing', 'Phishing'],
        datasets: [{
          backgroundColor: ['#28a745', '#dc3545'],
          data: [.5, .5]
        }]
      },
      options: {
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        layout: {
          padding: {
            top: 20,
            bottom: 20
          }
        }
      }
    });
    // end probability chart

    $('#phishing-dataset').DataTable({
      ajax: `${process.env.REACT_APP_BACKEND_API}/history`,
      columns: [
        {
          data: ({url}) => {
            return `<a href="javascript:void(0);" title="${url}" data-toggle="modal" data-target="#site-detail" data-url="${url}"><p class="history-url">${url}</p></a>`
          }
        },
        {
          data: 'date',
          className: 'text-center history-date'
        },
        {
          className: 'text-center',
          data: ({prediction}) => {
            return `<span class="badge badge-${prediction === 1 ? 'danger' : 'success'}">${prediction === 1 ? 'phishing' : 'non-phishing'}</span>`
          }
        },
      ],
      processing: true,
      serverSide: true,
      searching: false,
      ordering: false,
      pageLength: 7,
      lengthChange: false
    })

    $('#site-detail').on('show.bs.modal', async e => {
      const button = $(e.relatedTarget)
      const url = encodeURIComponent(button.data('url'));
      const {data} = await axios.get(`${process.env.REACT_APP_BACKEND_API}/history/${url}`);
      const {features, result, screenshot} = data;
      const featureName = {
        ip: "IP address",
        simbol_at: "Sombol @",
        afiks: "Total affix",
        usia_domain: "Domain ages",
        organization: "Organization",
        privasi: "Domain private",
        url_long: "URL long",
        url_https: "HTTPs",
        alexarank: "Alexa rank",
        js: "Total javascript files",
        page_score: "PSI Score"
      }

      $('#site-url').attr('href', decodeURIComponent(url))

      Object
        .entries(features).forEach(([key, value]) => {
        $(`.modal-body #${key} strong`).text(featureName[key]);
        $(`.modal-body #${key} span`).text(value);
      })
      $(`.modal-body #result p`).html(result.predict === 1 ?
        '<span class="badge badge-danger">Phishing</span>' :
        '<span  class="badge badge-success">Non-phishing</span>'
      );

      await this.removeData(chart)
      this.addData(chart, result.proba[0], result.proba[1])

      $('#site-screenshot').attr('src', screenshot);
    })
  }

  removeData = (chart) => {
    chart.data.datasets.forEach((dataset) => {
      dataset.data.pop()
      dataset.data.pop()
    });
    chart.update();
  }

  addData = (chart, nonPhishing, phishing) => {
    chart.data.datasets.forEach((dataset) => {
      dataset.data.push(nonPhishing)
      dataset.data.push(phishing)
    });
    chart.update();
  }

  render() {
    return (
      <div className="d-flex flex-column h-100 bd-highlight">
        <div className="modal fade" id="site-detail" tabIndex="-1" role="dialog" aria-labelledby="site-detail-label"
             aria-hidden="true">
          <div className="modal-dialog modal-xl" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="row row-cols-2 row-cols-lg-6">
                  {
                    featureList.map(val => (
                      <FeatureDetail key={val} id={val}/>
                    ))
                  }
                  <div className="col text-center" id="result">
                    <strong className="feature-title">Result</strong>
                    <p>a result</p>
                  </div>
                </div>
                <div className="row border-top pt-3">
                  <div className="col-12 col-lg-6" id="probability">
                    <h4 className="text-center">Probability</h4>
                    <canvas id="probability-chart"></canvas>
                  </div>
                  <div className="col-12 col-lg-6">
                    <h4 className="text-center">Screenshot</h4>
                    <img alt="Site screenshot" id="site-screenshot" className="col-12 py-3"/>
                  </div>
                </div>
              </div>
              <div className="modal-footer border-0">
                <a href="https://google.com" target="_blank" id="site-url" rel="noopener noreferrer">
                  <button type="button" className="btn btn-primary">Open URL <FaExternalLinkAlt className="ml-2"/></button>
                </a>
                <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <NavBar/>
        <main role="main" className="flex-fill bd-highlight">
          <div className="container-fluid px-5">
            <div className="row">
              <div className="col">
                <h2 className="text-center"><b>History</b></h2>
                <div className="table-responsive pt-2">
                  <table className="table table-striped table-bordered" id="phishing-dataset">
                    <thead className="text-center">
                    <tr>
                      <th>Site URL</th>
                      <th>Date</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </main>
        <WithLoveFooter/>
      </div>
    )
  }
}

export default History;