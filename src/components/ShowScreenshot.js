import React, {Component} from 'react';
import {BarLoader} from 'react-spinners';

class ShowScreenshot extends Component {
  render() {
    const {screenshot} = this.props;
    if (screenshot !== 'abc') {
      return (<img className="img-fluid" alt="Site screenshot" width={"100%"} src={screenshot}/>)
    } else {
      return (
        <div className="row align-content-center" style={{height: 280}}>
          <div className="col-12" align="center">
            <BarLoader color={'#858585'} height={10} width={250} loading={true}/>
          </div>
        </div>
      )
    }
  }
}

export default ShowScreenshot;