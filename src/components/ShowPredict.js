import React, {Component} from 'react';

class ShowPredict extends Component {
  render() {
    const {predict} = this.props;

    return (
      <h5 className="text-center"><b>This site is {predict === 1 ? (
        <span className="badge badge-danger">Phishing</span>) : (
        <span className="badge badge-success">Non-phishing</span>)} </b></h5>
    );
  }
}

export default ShowPredict;