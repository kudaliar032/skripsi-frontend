import React, {Component} from 'react';
import ShowProba from "./ShowProba";
import ShowFeature from "./ShowFeature";
import ShowScreenshot from "./ShowScreenshot";
import logo from '../img/with-text.svg';
import {Link, useHistory} from 'react-router-dom';
import axios from "axios";
import qs from 'query-string';
import {HashLoader} from "react-spinners";
import Footer from "./Footer";
import ShowPredict from "./ShowPredict";
import {UrlAlert, DetectAlert} from "./MySwal";
import NProgress from 'nprogress';

NProgress.configure({
  showSpinner: false,
  trickleSpeed: 500,
  minimum: 0.3
})

const DetectButton = props => {
  const history = useHistory();

  async function clickDetect() {
    try {
      props.inProgress('start-spinner');
      const url = encodeURIComponent(props.urlValue);
      await axios.get(`/url-check?url=${url}`);
      history.push(`/result?url=${url}`);
      props.detectSite();
    } catch (e) {
      props.inProgress('stop-spinner')
      UrlAlert();
    }
  }

  return (
    <button
      className="btn btn-secondary"
      type="submit"
      id="input-url-site"
      onClick={clickDetect}
      disabled={props.urlValue === ""}
    >Detect
    </button>

  );
};

class Result extends Component {
  constructor(props) {
    super(props);
    const url = qs.parse(this.props.location.search).url;

    this.state = {
      url,
      features: {
        ip: -1,
        simbol_at: -1,
        afiks: 0,
        usia_domain: 0,
        organization: -1,
        privasi: -1,
        url_long: 0,
        url_https: -1,
        alexarank: -1,
        js: 0,
        page_score: 0
      },
      proba: {
        0: 0.5,
        1: 0.5
      },
      predict: 0,
      screenshot: 'abc',
      isLoading: false
    };
  }

  componentDidMount() {
    this.detectSite();
  }

  changeUrl(e) {
    this.setState({url: e.target.value});
  }

  async detectSite() {
    try {
      const {url} = this.state;
      await this.inProgress('start');
      const {data} = await axios('/predict', {
        params: {url}
      });
      this.setState({...data});
      await this.inProgress('finish');
    } catch (e) {
      DetectAlert();
      this.inProgress('finish');
    }
  }

  inProgress(state) {
    if (state === 'start') {
      NProgress.start();
      this.setState({isLoading: true})
    } else if (state === 'finish') {
      NProgress.done();
      NProgress.remove();
      this.setState({isLoading: false});
    } else if (state === 'start-spinner') {
      this.setState({isLoading: true});
    } else if (state === 'stop-spinner') {
      this.setState({isLoading: false});
    }
  }

  render() {
    const {features, predict, proba, isLoading, screenshot} = this.state;
    return (
      <div className="d-flex flex-column h-100 bd-highlight">
        <header className="py-3 bd-highlight">
          <div className="row mx-2 align-items-center">
            <div className="col-12 col-lg-2">
              <Link to="/">
                <div className="d-flex">
                  <img src={logo} alt="Phishycho logo" className="img-fluid col-8 col-lg-12 mb-4 mb-lg-0 mx-auto"/>
                </div>
              </Link>
            </div>
            <div className="col-12 col-lg-9">
              <div className="input-group input-group-lg">
                <input
                  type="text"
                  value={this.state.url}
                  onChange={this.changeUrl.bind(this)}
                  className="form-control"
                  placeholder="Type URL here..."
                  aria-label="Type URL here..."
                  aria-describedby="input-url-site"
                />
                <div className="input-group-append">
                  <DetectButton inProgress={this.inProgress.bind(this)} detectSite={this.detectSite.bind(this)}
                                urlValue={this.state.url}/>
                </div>
              </div>
            </div>
          </div>
        </header>

        <main role="main" className="flex-fill bd-highlight">
          <div className="container-fluid h-100">
            {
              isLoading ? (
                <div className="row h-100 align-content-center">
                  <div className="col" align="center">
                    <HashLoader
                      size={75}
                      color={"#16A39C"}
                      loading={this.state.isLoading}
                    />
                  </div>
                </div>
              ) : (
                <div className="row mb-4">
                  <div className="col-12">
                    <ShowFeature features={features} className="mb-4 mt-2 mx-2"/>
                  </div>
                  <div className="col-12 col-lg-6">
                    <div className="ml-lg-5">
                      <h4 className="text-center">PROBABILITY</h4>
                      <ShowProba proba={proba}/>
                      <ShowPredict predict={predict}/>
                    </div>
                  </div>
                  <div className="col-12 col-lg-6">
                    <div className="mr-lg-5 mt-4 mt-lg-0">
                      <h4 className="text-center">SCREENSHOT</h4>
                      <ShowScreenshot screenshot={screenshot}/>
                    </div>
                  </div>
                </div>
              )
            }
          </div>
        </main>
        <Footer/>
      </div>
    )
  }
}

export default Result;