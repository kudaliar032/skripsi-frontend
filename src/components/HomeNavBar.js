import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

class HomeNavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light">
        <button className="navbar-toggler m-3 m-lg-0" type="button" data-toggle="collapse" data-target="#homeNavbar"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse pr-lg-5" id="homeNavbar">
          <ul className="navbar-nav ml-auto mr-lg-5 my-2 text-center">
            <li className="nav-item">
              <NavLink className="nav-link" to="/dataset">Dataset</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/history">History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/about">About</NavLink>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}

export default HomeNavBar;