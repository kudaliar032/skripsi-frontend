import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import {FaHeart} from 'react-icons/fa';

class Footer extends Component {
  render() {
    return (
      <footer className="mt-auto bd-highlight">
          <nav className="navbar navbar-expand navbar-light bg-light">
            <div className="navbar-nav mr-auto ml-5 d-none d-lg-block">
              <p className="m-0">Made with <FaHeart color={'#e25555'}/> by <a target="_blank" rel="noopener noreferrer" href="https://github.com/kudaliar032">Aditya Rahman</a></p>
            </div>
            <div className="navbar-nav mx-auto mr-lg-0 ml-lg-auto mr-5">
              <NavLink className="nav-link" to="/dataset">Dataset</NavLink>
              <NavLink className="nav-link" to="/history">History</NavLink>
              <NavLink className="nav-link" to="/about">About</NavLink>
            </div>
          </nav>
      </footer>
    );
  }
}

export default Footer;