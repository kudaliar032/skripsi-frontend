import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import logo from "../img/with-text.svg";

class NavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light">
        <NavLink className="navbar-brand p-3 p-lg-0" exact to="/">
          <img alt="Phishyco logo" src={logo} height={50}/>
        </NavLink>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav mr-auto my-lg-2 text-center">
            <li className="nav-item">
              <NavLink className="nav-link" exact to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" exact to="/dataset">Dataset</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" exact to="/history">History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/about">About</NavLink>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;