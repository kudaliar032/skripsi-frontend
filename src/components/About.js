import React, {Component} from 'react';
import NavBar from "./NavBar";
import WithLoveFooter from "./WithLoveFooter";
import mahasiswa from '../img/mahasiswa.jpg';
import pembimbing1 from '../img/pembimbing-1.jpg';
import pembimbing2 from '../img/pembimbing-2.jpg';

class About extends Component {
  render() {
    return (
      <div className="d-flex flex-column h-100 bd-highlight">
        <NavBar/>
        <main role="main" className="flex-fill bd-highlight">
          <div className="container mt-4">
            <div className="row justify-content-center mb-5">
              <div className="col-11 col-lg-8 text-center">
                <h5>Phishyco is an application that can be used to detect a phishing or non-phishing site. This application was created using machine learning model with the Random Forest algorithm</h5>
              </div>
            </div>
            <div className="row justify-content-center text-center">
              <div className="col-12 col-lg-3">
                <img alt="Student" src={mahasiswa} className="img-fluid mb-4" width={100}/>
                <h6>Aditya Rahman</h6>
                <p>Student<br/>160535611825</p>
              </div>
              <div className="col-12 col-lg-3">
                <img alt="Supervisor I" src={pembimbing1} className="img-fluid mb-4" width={100}/>
                <h6>Harits Ar Rosyid, S.T., M.T., Ph.D.</h6>
                <p>Supervisor I<br/>198108112009121003</p>
              </div>
              <div className="col-12 col-lg-3">
                <img alt="Supervisor II" src={pembimbing2} className="img-fluid mb-4" width={100}/>
                <h6>Muhammad Iqbal Akbar, S.ST., M.MT.</h6>
                <p>Supervisor II<br/>198810242015041002</p>
              </div>
            </div>
          </div>
        </main>
        <WithLoveFooter/>
      </div>
    )
  }
}

export default About;