import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import axios from 'axios';

axios.defaults.baseURL = process.env.REACT_APP_BACKEND_API;

ReactDOM.render(<App />, document.getElementById('root'));