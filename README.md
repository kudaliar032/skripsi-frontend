# Phishyco

## Stack
- Node.js 12.x (with npm)

## How to deploy
### Development
- `cp .env.example .env`, and modified with your environments
- `npm install`
- `npm run dev`

### Production
- `cp .env.example .env`, and modified with your environments
- `npm intall`
- `npm run build`
- You can run apps with `npm start` or hosted directory `build` with web server like`nginx or apache2`

---
Make with :heart: by. [Aditya Rahman](https://www.linkedin.com/in/adityarahman032/)